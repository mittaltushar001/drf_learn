from django.db import models

# Create your models here.

'''
class employee(models.Model):
    fn = models.CharField(max_length=100)
    ln = models.CharField(max_length=100)
    emp_id = models.IntegerField()
     
    def __str__(self):
        return self.fn 
'''

class Article(models.Model):
    title = models.CharField(max_length=100)
    author = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title